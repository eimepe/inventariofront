import {
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from "@ant-design/icons";
import { getDirectusClient } from "../lib/directus";
import { Layout, Menu, theme, Breadcrumb } from "antd";
import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const { Header, Content, Footer, Sider } = Layout;

const items2 = [UserOutlined, LaptopOutlined, NotificationOutlined].map(
  (icon, index) => {
    const key = String(index + 1);
    return {
      key: `sub${key}`,
      icon: React.createElement(icon),
      label: `subnav ${key}`,
      children: new Array(4).fill(null).map((_, j) => {
        const subKey = index * 4 + j + 1;
        return {
          key: subKey,
          label: `option${subKey}`,
        };
      }),
    };
  }
);

const Layouts = (props) => {
  const [items, setItems] = useState([
    {
      key: 1,
      label: "inicio",
      icon: <UserOutlined />,
      children: [
        {
          key: 2,
          label: "inicio",
          icon: <UserOutlined />,
        },
      ],
    },
  ]);

  const getMenu = async () => {
    const directus = await getDirectusClient();
    const response = await directus.items("menu").readByQuery({
      fields: ["*.*"],
    });
    const menuarray = [];

    response.data.map((res) => {
      if (res.espadre == true) {
        const arraychildren = [];

        res.children.map((chil) => {
          arraychildren.push({
            key: chil.id,
            label: chil.label,
          });
        });

        menuarray.push({
          key: res.id,
          label: res.label,

          children: res.children.length > 0 ? arraychildren : null,
        });
      }
    });
    setItems(menuarray);
  };
  useEffect(() => {
    getMenu();
  }, []);

  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout hasSider>
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
        style={{
          overflow: "auto",
          height: "100vh",

          background: "#fff",
          left: 0,
          top: 0,
          bottom: 0,
        }}
      >
        <div
          style={{
            height: 32,
            margin: 16,
            background: "rgba(0, 0, 0, 0.2)",
          }}
        />
        <Menu mode="inline" defaultSelectedKeys={["1"]} items={items} />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
        </Header>
        <Content
          style={{
            margin: "10px 16px",
          }}
        >
          {props.children}
        </Content>
        <Footer
          style={{
            textAlign: "center",
          }}
        >
          Ant Design ©2023 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};
export default Layouts;
