import React from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";
import Auth from "./containers/Auth";
import Home from "./containers/Home";

function Routers(props) {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/login" element={<Auth />} />
    </Routes>
  );
}

export default Routers;
