import { Col, Image, Row, Form, Input, Checkbox, Button } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getDirectusClient } from "../lib/directus";
function Auth(props) {
  const [authenticated, setAuthenticated] = useState(false);
  const navigate = useNavigate();
  const onFinish = async (values) => {
    console.log("holaa");
    const directus = await getDirectusClient();

    // Try to authenticate with token if exists
    await directus.auth
      .refresh()
      .then((res) => {
        setAuthenticated(true);
      })
      .catch((err) => {
        console.log(err);
      });

    // Let's login in case we don't have token or it is invalid / expired
    if (authenticated == false) {
      const email = values.username;
      const password = values.password;

      await directus.auth
        .login({ email, password })
        .then(() => {
          authenticated = true;
        })
        .catch(() => {
          window.alert("Invalid credentials");
        });
    } else {
      navigate("/");
    }

    // GET DATA

    // After authentication, we can fetch data from any collections that the user has permissions to.
  };

  const validarlogin = async () => {
    const directus = await getDirectusClient();

    await directus.auth
      .refresh()
      .then((res) => {
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    validarlogin();
  }, []);

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Row>
      <Col xs={24} lg={12}>
        <Image style={{ height: "100vh" }} preview={false} src="login.jpg" />
      </Col>{" "}
      <Col
        xs={24}
        lg={12}
        style={{
          display: "flex",
          alignItems: "center",
          alignContent: "center",
        }}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Usuario"
            name="username"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Contraseña"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Ingresar
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
}

export default Auth;
