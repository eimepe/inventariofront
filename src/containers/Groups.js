import {
  Table,
  Button,
  Modal,
  Space,
  Form,
  Input,
  Checkbox,
  Switch,
  message,
  InputNumber,
} from "antd";
import { CheckOutlined, CloseOutlined, PlusOutlined } from "@ant-design/icons";

import React, { useEffect, useState } from "react";
import { getDirectusClient } from "../lib/directus";
import Layouts from "../components/Layouts";

const Groups = () => {
  const [filter, setFilter] = useState([]);
  const [hasCode, setHasCode] = useState(false);
  const [Code, setCode] = useState(false);

  const [form] = Form.useForm();

  const handleCheckboxChange = (e) => {
    setHasCode(e.target.checked);
  };

  const handleCheckbox = (e) => {
    setCode(e.target.checked);
  };

  const [data, setData] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [ModalOpen, setModalOpen] = useState(false);
  const [OpenEdit, setOpenEdit] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const Modalopen = () => {
    setModalOpen(true);
  };
  const openEdit = () => {
    setOpenEdit(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
    setModalOpen(false);
    setOpenEdit(false);
  };

  //Get
  const GetGroups = async () => {
    const directus = await getDirectusClient();
    const datos = await directus.items("grupos").readByQuery({
      fields: "*.*.*",
      limit: -1,
    });
    setData(datos.data);
  };

  //Post
  const onFinish = async (values) => {
    const directus = await getDirectusClient();
    const res = await directus.items("grupos").createOne({
      nombre: values.nombre,
      codebar: values.codebar,
      iccd: values.iccd,
      codebarmax: values.codebarmax,
      codebarmin: values.codebarmin,
      iccdmax: values.iccdmax,
      iccdmin: values.iccdmin,
      impuesto: values.impuesto,
      afectainventario: values.afectainventario
    });

    if (res.id > 0) {
      message.success("Grupo creado");
      GetGroups();
    } else {
      message.error("Upss!!, algo salio mal :(");
    }
  };

  //Delete
  const DeleteGroup = async (id) => {
    const directus = await getDirectusClient();
    await directus.items("grupos").deleteOne(id);
    GetGroups();
  };

  //Put

  useEffect(() => {
    GetGroups();
  }, []);

  const columns = [
    {
      title: "Name",
      dataIndex: "nombre",
      key: "nombre",
    },
    {
      title: "Code",
      dataIndex: "codebar",
      key: "codebar",
    },
    {
        title: "Code",
        dataIndex: "codebarmax",
        key: "codebarmax",
      },
      {
        title: "Code",
        dataIndex: "codebarmin",
        key: "codebarmin",
      },
    {
      title: "Iccid",
      dataIndex: "iccid",
      key: "iccid",
    },
    {
      title: "Iccidmax",
      dataIndex: "iccidmax",
    },
    {
        title: "Iccidmin",
        dataIndex: "iccidmin",
      },
    {
        title: "Impuesto",
        dataIndex: "impuesto"
    },
    {
        title: "Estado",
        dataIndex: "estado"
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        setFilter(record),
        (
          <Space size="middle">
            <Button
              type="link"
              onClick={() => {
                setOpenEdit(true);
                form.setFieldsValue({ nombre: record.nombre });
                form.setFieldsValue({ impuesto: record.impuesto });
                form.setFieldsValue({ codebar: record.codebar });
                form.setFieldsValue({ codebarmax: record.codebarmax });
                form.setFieldsValue({ codebarmin: record.codebarmin });
                form.setFieldsValue({ iccd: record.iccd });
                form.setFieldsValue({ iccidmax: record.iccidmax });
                form.setFieldsValue({ iccdmin: record.iccdmin });
                form.setFieldsValue({ estado: record.estado });
                form.setFieldsValue({ afectainventario: record.afectainventario });
              }}
            >
              Edit
            </Button>
            <Button type="link" onClick={showModal}>
              View
            </Button>
            <Modal
              title="My data"
              open={isModalOpen}
              onOk={handleOk}
              onCancel={handleCancel}
            >
              <p>{record.nombre}</p>
            </Modal>

            <Button type="link" danger onClick={() => DeleteGroup(record.id)}>
              Delete
            </Button>
          </Space>
        )
      ),
    },
  ];

  const EditGroup = async (values) => {
    const directus = await getDirectusClient();
    const update = await directus.items("grupos").updateOne(filter.id, {
      nombre: values.nombre,
      codebar: values.codebar,
      iccd: values.iccd,
      codebarmax: values.codebarmax,
      codebarmin: values.codebarmin,
      iccdmax: values.iccdmax,
      iccdmin: values.iccdmin,
      impuesto: values.impuesto,
      afectainventario: values.afectainventario
    });
    if (update.id > 0) {
      message.success("Grupo Editado");
      GetGroups();
    } else {
      message.error("Upss!!, algo salio mal :(");
    }
  };

  return (
    <Layouts>
     
      <Table dataSource={data} columns={columns}  title={() => <Button icon={<PlusOutlined />} onClick={Modalopen}>Nuevo</Button>}/>;
      {/* Edit Form */}
      <Modal open={OpenEdit} footer={null} onCancel={handleCancel}>
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={{
            codebar: false,
            iccd: false,
            estado: false,
            codebarmax: 0,
            codebarmin: 0,
            iccdmax: 0,
            iccdmin: 0,
          }}
          autoComplete="off"
          onFinish={EditGroup}
        >
          <Form.Item
            name="nombre"
            rules={[
              {
                required: true,
                message: "Este campo es obligatorio",
              },
            ]}
          >
            <Input placeholder="Nombre del grupo" />
          </Form.Item>
          <Form.Item
            name="impuesto"
            label="Impuesto"
            rules={[
              { required: true, message: "Este es un campo obligatorio" },
            ]}
          >
            <InputNumber
              min={0}
              max={100}
              step={0.01}
              formatter={
                (value) => `${value}%` // Agrega el símbolo de porcentaje después del número ingresado
              }
            />
          </Form.Item>
          <Form.Item name="codebar" valuePropName="checked">
            <Checkbox onChange={handleCheckboxChange}>Tiene codigo?</Checkbox>
          </Form.Item>

          {hasCode && (
            <>
              <Form.Item
                label="Maximo"
                name="codebarmax"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Código de barras máximo" />
              </Form.Item>

              <Form.Item
                label="Minimo"
                name="codebarmin"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Código de barras mínimo" />
              </Form.Item>
            </>
          )}

          <Form.Item name="iccd" valuePropName="checked">
            <Checkbox onChange={handleCheckbox}>Iccid?</Checkbox>
          </Form.Item>

          {Code && (
            <>
              <Form.Item
                label="Maximo"
                name="iccidmax"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Código máximo" />
              </Form.Item>

              <Form.Item
                label="Minimo"
                name="iccidmin"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Codigo minimo" />
              </Form.Item>
            </>
          )}

          <Form.Item name="estado" label="Estado">
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          </Form.Item>

          <Form.Item name="afectainventario" label="Afecta Inventario">
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Editar
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {/* Created Form */}
      <Modal open={ModalOpen} onCancel={handleCancel} footer={false}>
        <Form
          name="basic"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={{
            codebar: false,
            iccd: false,
            estado: false,
            codebarmax: 0,
            codebarmin: 0,
            iccdmax: 0,
            iccdmin: 0,
          }}
          autoComplete="off"
          onFinish={onFinish}
        >
          <Form.Item
            name="nombre"
            rules={[
              {
                required: true,
                message: "Este campo es obligatorio",
              },
            ]}
          >
            <Input placeholder="Nombre del grupo" />
          </Form.Item>
          <Form.Item
            name="impuesto"
            label="Impuesto"
            rules={[
              { required: true, message: "Este es un campo obligatorio" },
              { type: "number", message: "Ingrese un número válido" },
            ]}
          >
            <InputNumber
              min={0}
              max={100}
              step={0.01}
              formatter={
                (value) => `${value}%` // Agrega el símbolo de porcentaje después del número ingresado
              }
            />
          </Form.Item>
          <Form.Item name="codebar" valuePropName="checked">
            <Checkbox onChange={handleCheckboxChange}>Tiene codigo?</Checkbox>
          </Form.Item>

          {hasCode && (
            <>
              <Form.Item
                label="Maximo"
                name="codebarmax"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Código de barras máximo" />
              </Form.Item>

              <Form.Item
                label="Minimo"
                name="codebarmin"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Código de barras mínimo" />
              </Form.Item>
            </>
          )}

          <Form.Item name="iccd" valuePropName="checked">
            <Checkbox onChange={handleCheckbox}>Iccid?</Checkbox>
          </Form.Item>

          {Code && (
            <>
              <Form.Item
                label="Maximo"
                name="iccidmax"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Código máximo" />
              </Form.Item>

              <Form.Item
                label="Minimo"
                name="iccidmin"
                rules={[
                  { required: true, message: "Este es un campo obligatorio" },
                ]}
              >
                <InputNumber placeholder="Codigo minimo" />
              </Form.Item>
            </>
          )}

          <Form.Item name="estado" label="Estado">
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
              defaultChecked={false}
            />
          </Form.Item>

          <Form.Item name="afectainventario" label="Afecta Inventario">
            <Switch
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
              defaultChecked={false}
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Crear
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </Layouts>
  );
};

export default Groups;
