import { Button } from "antd";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Layouts from "../components/Layouts";
import { counterAdd } from "../features/counter/counterSlice";
import { getDirectusClient } from "../lib/directus";

function Home(props) {
  const dispatch = useDispatch();

  const peticion = async () => {
    const directus = await getDirectusClient();

    const response = await directus.items("grupos").readByQuery({
      fields: ["*"],
    });
    dispatch(counterAdd([4, 5, 6]));
    console.log(response);
  };

  useEffect(() => {
    peticion();
  }, []);
  return (
    <Layouts>
      <Button>Hola</Button>
    </Layouts>
  );
}

export default Home;
